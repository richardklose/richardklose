## Hey, I'm Richard! 👋

I'm a site reliability engineer in the platforms and applications team at German Edge Cloud. My professional work focus is all about automation in development and operations. This README is all about my personal work, my thechnical skills and me as a coworker or technical sidekick.

## About me

I've been a _professional_ software and operations engineer since 2014. I started digging into computers in the 90s with a pile of old i386 PCs and some free suse Linux CDs. This somhow brought me to programming, espacially web programming.

After school I studied Media Systems at the University of Applied Sciences in Hamburg from 2010 to 2014, with a main focus on game development. However, during this time I realized, that this might not be exactly what I wanted. So I left Hamburg after my Bachelor Thesis and started my professional carrer in tech.

Since then I've worked for companies in local, as well as international business, as well as some voluntary projects. I moved through several web stacks, have written apps in different mobile frameworks, was part of team that designed a Linux-based embedded PBX from scratch and succesfully introduced devops and automation toolchains to people that have never heard of this before. This led me to German Edge Cloud in April 2022, where I'm now juggling around with virtual machines and containers in several cloud platforms and cloud technologies.

I was born in a small city in eastern Germany, but my parents moved through the whole country several times. Meanwhile I am married, have three kids and we live in Lemgo, about 30 minutes east of Bielefeld, Germany.
After work I am mostly busy renovating our house or having time with my family. At least two times a year I participate on youth camps of the CVJM/YMCA together with my family and friends.

## What I do

My official job title is site reliability engineer, but I am still not sure if that fits exactly. However, I dont't really care about job titles. Here is what I actually do or have done in the past:

* Web development
* App development
* Protocol engineering
* Development project management
* Automation and DevOps conception and implementation
* Automated API testing and test framework development
* Cloud architecture design and automation

I'm also mostly always up for some fun time with my team. Good releationships to everyone I work with is something, but not the only thing, that is important for good work!
